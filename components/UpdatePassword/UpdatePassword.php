<?php 
    include '../_Master/_header.php';
    // if(isset($Role)){
    //     if ($Role != 0) {
    //         echo "<script type='text/javascript'>window.location.href = '../PermissionDenied/PermissionDenied.php';</script>";  
    //     }
    // }else{
    //     echo "<script type='text/javascript'>window.location.href = '../Home/Index.php';</script>";  
    // } 
?>
<!-- POST  -->
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", window.location);
                        $('#myModalAlert').modal('show');              
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'รหัสผ่านปัจจุบันหรือรหัสผ่านใหม่ไม่ถูกต้อง';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                             
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'รหัสผ่านปัจจุบันหรือรหัสผ่านใหม่ไม่ถูกต้อง';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                              
                    }            
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "ประเภทรถ"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">ประเภทรถ</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>ประเภทรถ </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="#" class="form-horizontal" id="addForm">
                            <div class="form-body">                               

                                    <div class="form-group">
                                        <input type="hidden" name="func" class="form-control" value="Password">
                                        <label class="control-label col-md-3">รหัสผ่านเดิม <span class="colorRed">*</span></label>
                                        <div class="col-md-4">
                                            <input type="password" id="oldPassword" name="oldPassword" class="form-control col-md-4" required/>
                                        </div>
                                     </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">รหัสผ่านใหม่ <span class="colorRed">*</span></label>
                                        <div class="col-md-4">
                                            <input type="password" id="newPassword" name="newPassword" class="form-control" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">ยืนยันรหัสผ่านใหม่ <span class="colorRed">*</span></label>
                                        <div class="col-md-4">
                                            <input type="password" id="newPassword2" name="newPassword2" class="form-control" required/> 
                                        </div>
                                    </div>
                                        <input type="hidden" name="Id" class="form-control" value="<?php echo $row['Id'] ?>" required>
                                        <input type="hidden" name="Username" value="<?php echo $Username; ?>">
                                    <div class="form-group">
                                         <div class="margin-top-10 col-md-4 col-md-offset-3" >
                                             <button class="btn green"> เปลี่ยนรหัสผ่าน </button>
                                        </div>
                                    </div>

                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END EXTRAS PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>