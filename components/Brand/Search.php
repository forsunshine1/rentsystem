<?php 
    include '../_Master/_header.php';
    if(isset($Role)){
        if ($Role == 2) {
            echo "<script type='text/javascript'>window.location.href = '../PermissionDenied/PermissionDenied.php';</script>";  
        }
    }else{
        echo "<script type='text/javascript'>window.location.href = '../Login/Login.php';</script>";  
    }
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Brand']) != '') {
    // $sql = "SELECT * FROM faculty WHERE FacultyCode LIKE '%" . $_GET['FacultyCode'] . "%' "OR" FacultyName LIKE '%" . $_GET['FacultyName'] . "%' "; 
        if (isset($_GET['Brand']) != "") {
            $Brand = "brand.Name LIKE'%" . $_GET['Brand'] . "%'";
        }else{
            $Brand = "";
        }

        $sql = "SELECT brand.Id , 
                brand.Name ,
                brand.Description ,
                brand.YearId,
                Year.Year,
                brand.Created FROM brand LEFT join year on brand.YearID = year.Id WHERE $Brand order by Name ASC";

        $result = mysqli_query($con,$sql); 
    }else{
        $sql = "SELECT brand.Id , 
                brand.Name ,
                brand.Description ,
                brand.YearId,
                Year.Year,
                brand.Created FROM brand LEFT join year on brand.YearID = year.Id";  
        $result = mysqli_query($con,$sql); 
    }
    mysqli_close($con);
?>
<!-- POST  -->

<script type="text/javascript">
    document.title = "ประเภทยี่ห้อรถ"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">ประเภทยี่ห้อรถ</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="portlet light bordered" id="addPanel" >
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-search font-red-sunglo"></i>
                                    <span class="caption-subject font-red-sunglo bold uppercase">ค้นหา</span>
                                </div>
                            <div class="actions">
                                <a class="btn yellow" href="Create.php">สร้างข้อมูล</a>
                            </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal" id="addForm">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">ประเภทยี่ห้อรถ</label>
                                            <div class="col-md-4">
                                                <input type="text" name="Brand" class="form-control" placeholder="ประเภทยี่ห้อรถ">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn blue">ค้นหา</button>
                                            </div>
                                        </div>
                                    </div>                                         
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="fa fa-table font-red-sunglo"></i>
                            <span class="caption-subject font-red-sunglo bold uppercase">ผลการค้นหา</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> ปี </th>
                                    <th> ยี่ห้อรถ </th>
                                    <th> รายละเอียด </th>
                                    <th> วันที่สร้าง </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $i = 1;
                                    while ($row = mysqli_fetch_assoc($result)) {  
                                ?>
                               <tr>  
                                    <td><?php echo $row["Year"]; ?></td> 
                                    <td><?php echo $row["Name"]; ?></td>  
                                    <td><?php echo $row["Description"]; ?></td> 
                                    <td><?php echo date('d/m/Y', strtotime($row['Created']));?></td>  
                                    <td>
                                        <?php echo '<a class="btn-xs btn  green"  href="Edit.php?Id='. $row['Id'] .'" >แก้ไข</a>'; ?>
                                        <?php echo '<a class="btn-xs btn  red" data-toggle="modal" data-id="'. $row['Id'] .'" href="#myModal'. $row['Id'] .'"  >ลบ</a></td>'; ?>
                                    </td>                           
                                    <!-- Modal Delete-->
                                    <div class="modal fade bs-example-modal-sm" id="myModal<?php echo $row['Id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header" style="background-color: #f9243f;">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 style="color:#fff;" class="modal-title" id="myModalLabel">Alert</h4>
                                            </div>
                                            <div class="modal-body">
                                              ลบข้อมูล <?php echo $row['Name']?>
                                            </div>
                                            <div class="modal-footer">
                                              <a class="btn red" href="./Controller.php?Id=<?php echo $row['Id']?>&func=Delete">ตกลง</a>
                                              <a class="btn default" data-dismiss="modal">ยกเลิก</a>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- Modal Delete-->
                                    </div>
                                    <!-- Modal Delete-->                     
                                </tr>
                                <?php  
                                    };  
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>