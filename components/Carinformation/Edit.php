<?php 
    include '../_Master/_header.php';
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Id'])) {

        $Id = $_GET['Id'];
        $query = mysqli_query($con,"SELECT cr.Id , cr.CarId , cr.ModelCarId , model.Name  as ModelName , cr.PathImg1 ,cr.PathImg2 , cr.PathImg3 ,cr.PathImg4 , cr.PathImg5 FROM carinformation as cr left join model on cr.ModelCarId = model.Id WHERE cr.Id = '$Id' ");
        
        // $result = mysqli_query($con,$sql); 
        $row=mysqli_fetch_array($query,MYSQLI_ASSOC);
        
    }
?>
<!-- POST  -->
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", "./Search.php");
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }                
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "แก้ไขข้อมูลรถ"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">ข้อมูลรถ</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>แก้ไขข้อมูลรถ </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="#" class="form-horizontal" id="addForm">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">ทะเบียนรถ <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <input type="text" id="CarId" name="CarId" class="form-control" placeholder="ทะเบียนรถ" value="<?php if(isset($_GET['Id']) != ''){echo $row['CarId']; }?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">รุ่น <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <input type="text" id="CarId" name="CarId" class="form-control" placeholder="รุ่น" value="<?php if(isset($_GET['Id']) != ''){echo $row['ModelName']; }?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">รูปที่ 1</label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if ($row['PathImg1'] != ''): ?>
                                                    <img src="../../carimage/<?php echo $row['PathImg1']; ?>" alt="" />
                                                <?php endif ?>
                                                <?php if ($row['PathImg1'] == ''): ?>
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                <?php endif ?>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> เลือกรูป </span>
                                                    <span class="fileinput-exists"> เปลี่ยน </span>
                                                    <input type="file" name="img_member1"> 
                                                </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> ลบ 
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="OldImage1" class="form-control" value="<?php echo $row['PathImg1'] ?>" required>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">รูปที่ 2</label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if ($row['PathImg2'] != ''): ?>
                                                    <img src="../../carimage/<?php echo $row['PathImg2']; ?>" alt="" />
                                                <?php endif ?>
                                                <?php if ($row['PathImg2'] == ''): ?>
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                <?php endif ?>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> เลือกรูป </span>
                                                    <span class="fileinput-exists"> เปลี่ยน </span>
                                                    <input type="file" name="img_member2"> 
                                                </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> ลบ 
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="OldImage2" class="form-control" value="<?php echo $row['PathImg2'] ?>" required>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">รูปที่ 3</label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if ($row['PathImg3'] != ''): ?>
                                                    <img src="../../carimage/<?php echo $row['PathImg3']; ?>" alt="" />
                                                <?php endif ?>
                                                <?php if ($row['PathImg3'] == ''): ?>
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                <?php endif ?>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> เลือกรูป </span>
                                                    <span class="fileinput-exists"> เปลี่ยน </span>
                                                    <input type="file" name="img_member3"> 
                                                </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> ลบ 
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="OldImage3" class="form-control" value="<?php echo $row['PathImg3'] ?>" required>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">รูปที่ 4</label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if ($row['PathImg4'] != ''): ?>
                                                    <img src="../../carimage/<?php echo $row['PathImg4']; ?>" alt="" />
                                                <?php endif ?>
                                                <?php if ($row['PathImg4'] == ''): ?>
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                <?php endif ?>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> เลือกรูป </span>
                                                    <span class="fileinput-exists"> เปลี่ยน </span>
                                                    <input type="file" name="img_member4"> 
                                                </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> ลบ 
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="OldImage4" class="form-control" value="<?php echo $row['PathImg4'] ?>" required>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">รูปที่ 5</label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if ($row['PathImg5'] != ''): ?>
                                                    <img src="../../carimage/<?php echo $row['PathImg5']; ?>" alt="" />
                                                <?php endif ?>
                                                <?php if ($row['PathImg5'] == ''): ?>
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                <?php endif ?>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> เลือกรูป </span>
                                                    <span class="fileinput-exists"> เปลี่ยน </span>
                                                    <input type="file" name="img_member5"> 
                                                </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> ลบ 
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="OldImage5" class="form-control" value="<?php echo $row['PathImg5'] ?>" required>

                                <input type="hidden" name="func" class="form-control" value="Edit">
                                <input type="hidden" value="<?php echo $row['Id'] ?>" name="Id">
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn green"><i class="fa fa-check"></i> ตกลง</button>
                                        <a href="./Search.php" class="btn default">ยกเลิก</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END EXTRAS PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>