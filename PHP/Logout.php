<?php
	session_start();
	
	if(session_destroy()) // Destroying All Sessions
	{			
		header("Location: ../components/Login/Login.php"); // Redirecting To Home Page				
	}
?>