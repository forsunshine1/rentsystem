<?php

//สายงาน

$con=mysqli_connect("localhost","root","","rentsystemdb");
$con->set_charset("utf8");
$dbcon = mysqli_query($con,"SELECT * FROM reportemployee");
//สายงาน
// SELECT * FROM accounts INNER JOIN customers ON accounts.customer_id = customers.customer_id WHERE customers.customer_group_id = 1
$row = mysqli_fetch_array($dbcon);

require_once('../tcpdf/tcpdf.php');
class MYPDF extends TCPDF {

    //Page header
    public function Header() {

        $this->SetFont('angsanaupc', 'B', 16);


        $this->Cell(0, 0, 'วันที่ออกเอกสาร '.date("d/m/Y").'', 0, 0, 'R');


    }

     public function Footer() {
        $this->SetFont('angsanaupc', 'B', 16);

        $this->Cell(210, 0, 'หน้า '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 0, 'R');
     }

}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// remove default header/footer
$pdf->setHeaderData();
$pdf->setFooterData();
$pdf->setPrintHeader(true);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10, 20.6, 15,10);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(15);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('angsanaupc', '', 15);
$pdf->AddPage('P', 'A4');
$pdf->setPage(true);

$theader = '<h1 style="text-align:center;font-size:50px">รายงานสถานะพนักงานขับรถ</h1>';
$theader .= '<table border="1" width="100%">';
$theader .= '<theader>
           <tr>
             <th align="center" >ชื่อ-นามสกุล</th>
             <th align="center">จำนวนการเช่า</th>
             <th align="center">วันที่เช่าล่าสุด</th>
             <th align="center">สถานะผู้เช่า</th>

           </tr>
           </theader>';
$dbcon = mysqli_query($con,"SELECT * FROM reportemployee");
while($row = mysqli_fetch_array($dbcon))
{
  $DisplayStatus = $row['Status'] == '0' ? 'ปกติ': 'บัญชีดำ';
  $theader .= '
           <tr>
             <th align="center" >'.$row['Firstname'].' - '.$row['Lastname'].'</th>
             <th align="center">'.$row['countTransaction'].'</th>
             <th align="center">'.$row['currentDate'].'</th>
             <th align="center">'.$DisplayStatus.'</th>

           </tr>';
}



mysqli_close($con);
$tfooter = '</table>';
// Print text using writeHTMLCell()
$pdf->writeHTML($theader.$tfooter, true, false, false, false, '');
// ---------------------------------------------------------
//Close and output PDF document
$date = date("d/m/Y");
list($d_o,$m_o,$Y_o) = split('/',$date);
$Y_o = $Y_o + 543;
$pdf->Output(''.$d_o.'-'.$m_o.'-'.$Y_o.'.pdf', 'I');

 ?>
