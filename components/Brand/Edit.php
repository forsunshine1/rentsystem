<?php 
    include '../_Master/_header.php';
    if(isset($Role)){
        if ($Role == 2) {
            echo "<script type='text/javascript'>window.location.href = '../PermissionDenied/PermissionDenied.php';</script>";  
        }
    }else{
        echo "<script type='text/javascript'>window.location.href = '../Login/Login.php';</script>";  
    }
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Id'])) {

        $Id = $_GET['Id'];
        $query = mysqli_query($con,"SELECT brand.Id , 
                brand.Name ,
                brand.Description ,
                brand.YearId,
                Year.Year,
                brand.Created
                FROM brand left join Year on brand.YearId = Year.Id WHERE brand.Id = '$Id' ");
        
        // $result = mysqli_query($con,$sql); 
        $row=mysqli_fetch_array($query,MYSQLI_ASSOC);
        
    }
?>
<!-- POST  -->
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", "./Search.php");
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }                
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "เพิ่มยี่ห้อรถ"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">ประเภทยี่ห้อรถ</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>แก้ไขยี่ห้อรถ</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="#" class="form-horizontal" id="addForm">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">ยี่ห้อรถ <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <input type="text" id="Name" name="Name" class="form-control" placeholder="Name" value="<?php if(isset($_GET['Id']) != ''){echo $row['Id']; }?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">รายละเอียด</label>
                                    <div class="col-md-4">
                                        <input type="text" id="Description" name="Description" class="form-control" placeholder="Description" value="<?php if(isset($_GET['Id']) != ''){echo $row['Description']; }?>">
                                    </div>
                                </div>
                               
                                <?php 
                                    include '../../PHP/ConnectDB.php';

                                    $sql = "SELECT * FROM Year";  
                                    $listyear = mysqli_query($con,$sql); 
                                    
                                    mysqli_close($con);
                                ?>
                                <div class="form-group">
                                    <label for="single"  class="col-md-3 control-label">ปี</label>
                                    <div class="col-md-4">
                                        <select id="single" class="form-control select2" name="YearId">
                                            <?php if ($row['YearId'] != ''): ?>
                                                <option value='<?php echo $row['YearId']?>'><?php echo $row['Year']?></option>
                                            <?php endif ?>
                                            <?php if ($row['YearId'] == ''): ?>
                                                <option value="-1">Select category</option>
                                            <?php endif ?>
                                            <?php while ($row3=mysqli_fetch_assoc($listyear)) { ?>
                                                <?php if ($row['YearId'] != $row3['Id']): ?>
                                                    <option value="<?php echo $row3['Id']?>"><?php echo $row3['Year']?></option>
                                                <?php endif ?>
                                                
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="func" class="form-control" value="Edit">
                                <input type="hidden" value="<?php echo $row['Id'] ?>" name="Id">
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn green"><i class="fa fa-check"></i> ตกลง</button>
                                        <a href="./Search.php" class="btn default">ยกเลิก</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END EXTRAS PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>