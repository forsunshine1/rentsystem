<?php 
	header('Content-type: application/json; charset=utf-8');
		
	if (isset($_POST['func']) != '') {
		$func = $_POST['func'];
	}elseif (isset($_GET['func']) != '') {
		$func = $_GET['func'];
	}

	switch ($func) {
		case 'Create':
			Create();
			break;
		case 'Delete':
			Delete();
			break;
		case 'Edit':
			Edit();
			break;
		case 'Status':
			Status();
			break;			
		default:
			# code...
			break;
	}

	
	function Create()
	{
		include '../../PHP/ConnectDB.php';

		
		$Firstname 	= $_POST['Firstname'];
		$Lastname 	= $_POST['Lastname'];
		$IdCard  	= $_POST['IdCard'];
		$Address  	= $_POST['Address'];
		$Tel		 = $_POST['Tel'];
		if (isset($_FILES["img_member"])) {
			$temp = explode(".", $_FILES["img_member"]["name"]);
			$newfilename = round(microtime(true)) . '.' . end($temp);
			move_uploaded_file($_FILES["img_member"]["tmp_name"], "../../avatar/" . $newfilename);
			$imgpath = $newfilename ;
			
		}else{
			$imgpath = "";
		}
		


		if(isset($_POST['Firstname']))
		{
			$sql = "INSERT INTO employee (Firstname,Lastname,IdCard,Address,Tel,Img) VALUES ('$Firstname','$Lastname','$IdCard','$Address','$Tel','$imgpath')";

				mysqli_query($con, $sql);
				$response_array['status'] = 'success';  						

		}else{
		    
		    $response_array['status'] = 'error';
		} 
		mysqli_close($con);
		exit(json_encode($response_array));
	}

	function Edit()
	{
		include '../../PHP/ConnectDB.php';
		$Firstname 	= $_POST['Firstname'];
		$Lastname 	= $_POST['Lastname'];
		$IdCard  	= $_POST['IdCard'];
		$Address  	= $_POST['Address'];
		$Tel		 = $_POST['Tel'];
		$Id = $_POST['Id'];
		if (!empty($_FILES["img_member"]) AND $_FILES["img_member"]["name"] != '') {
			if (isset($_FILES["img_member"])) {
				$temp = explode(".", $_FILES["img_member"]["name"]);
				$newfilename = round(microtime(true)) . '.' . end($temp);
				move_uploaded_file($_FILES["img_member"]["tmp_name"], "../../avatar/" . $newfilename);
				$imgpath = $newfilename ;
				if (!empty($_FILES["img_member"]["name"]) AND $_FILES["img_member"]["name"] != '') {
					
					$valid =mysqli_query($con, "SELECT * FROM employee WHERE Id = '$Id'");
					$validImage = mysqli_fetch_array($valid,MYSQLI_ASSOC);

					if ($validImage['Img'] != '' || $validImage['Img'] != null) {
						unlink("../../avatar/" .$_POST['Avatar']);
					}

				}
			}else{
				$imgpath = "";
			}
		}else{
			if ($_POST['Avatar'] != null && $_POST['Avatar'] != "") {
				$imgpath = $_POST['Avatar'];
			}else{
				unlink("../../avatar/" .$_POST['Avatar']);
			}
		}

		
		//UPDATE
		if (isset($_POST['Id'])) {

			
			$sql = mysqli_query($con,"UPDATE employee SET Firstname = '$Firstname' , Lastname = '$Lastname' , IdCard = '$IdCard' , Address = '$Address' , Tel = '$Tel',Img = '$imgpath' WHERE Id='$Id'");
				$response_array['status'] = 'success';  

		}else{
			$response_array['status'] = 'error';
		}

		mysqli_close($con);
		exit(json_encode($response_array));
	}


	function Delete()
	{
		include '../../PHP/ConnectDB.php';
		if(isset($_GET['Id']))
		{
			$Id = $_GET['Id'];
			
			$sql = "DELETE FROM Employee WHERE Id='$Id'";

			mysqli_query($con, $sql);	

		}else{
		    echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
		} 

		mysqli_close($con);
		header('Location: ./Search.php');
	}

	function Status()
	{
		include '../../PHP/ConnectDB.php';
		

		if($_GET['Status'] == 1){
			$Status = 0;
		}else if($_GET['Status'] == 0){
			$Status = 1;
		}

		if(isset($_GET['Id']))
		{
			$Id = $_GET['Id'];
			
			$sql = mysqli_query($con,"UPDATE employee SET Status = '$Status' WHERE Id='$Id'");

			mysqli_query($con, $sql);	

		}else{
		    echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
		} 

		mysqli_close($con);
		header('Location: ./Search.php');
	}

	exit(json_encode($response_array));
 ?>