<?php 
     include '../../PHP/ConnectDB.php';
    
    mysqli_close($con);
?>
<style type="text/css">
    .loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../../assets/layouts/layout4/img/loading-spinner-blue.gif') 50% 50% no-repeat rgb(249,249,249);
  }

</style>
<script type="text/javascript">
    $(window).load(function() {
        $(".loader").fadeOut("slow");
    })
</script>
<div class="loader"></div>
<!-- BEGIN HEADER -->
     <div class="page-container">
    <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- BEGIN SIDEBAR -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                    <li class="nav-item start ">
                        <a href="../Login/Home.php">
                            <i class="icon-home"></i>
                            <span class="title">หน้าหลัก</span>
                        </a>
                        <a href="../Transaction/Search.php">
                            <i class="icon-wallet"></i>
                            <span class="title">ข้อมูลการเช่า</span>
                        </a>
                        <?php if (isset($Role)): ?>
                            <?php if ($Role == 0 || $Role == 1 ): ?>
                            <a href="../Cost/Search.php">
                                <i class="icon-wallet"></i>
                                <span class="title">ค่าเช่า</span>
                            </a>
                            <?php endif ?>   
                        <?php endif ?>
                        <?php if (isset($Role)): ?>
                            <?php if ($Role == 0 ): ?>
                                <a href="../Carinformation/Search.php">
                                    <i class="fa fa-car" style="margin-left: 1px;margin-right: 4px"></i>
                                    <span class="title">ข้อมูลรถ</span>
                                </a>
                            <?php endif ?>   
                        <?php endif ?>
                        <a href="../RepairInformation/Search.php">
                            <i class="icon-doc"></i>
                            <span class="title">ข้อมูลการซ่อมรถ</span>
                        </a>
                        <?php if (isset($Role)): ?>
                            <?php if ($Role == 0 || $Role == 1): ?>
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-note"></i>
                                    <span class="title">ข้อมูลหลัก</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start ">
                                        <a href="../Year/Search.php" class="nav-link ">
                                            <i class=""></i>
                                            <span class="title">ปีรถ</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="../Category/Search.php" class="nav-link ">
                                            <i class=""></i>
                                            <span class="title">ประเภทรถ</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="../Brand/Search.php" class="nav-link ">
                                            <i class=""></i>
                                            <span class="title">ประเภทยี่ห้อรถ</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="../Model/Search.php" class="nav-link ">
                                            <i class=""></i>
                                            <span class="title">รุ่นรถ</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="../Range/Search.php" class="nav-link ">
                                            <i class=""></i>
                                            <span class="title">ช่วงระยะเวลา</span>
                                        </a>
                                    </li>
                                </ul>
                            <?php endif ?>   
                        <?php endif ?>
                        <?php if (isset($Role)): ?>
                            <?php if ($Role == 0): ?>   
                                <a href="../User/Search.php">
                                    <i class="icon-user"></i>
                                    <span class="title">พนักงาน</span>
                                </a>
                            <?php endif ?>   
                        <?php endif ?>
                        <a href="../Employee/Search.php">
                            <i class="icon-users"></i>
                            <span class="title">ข้อมูลพนักงานขับรถ</span>
                        </a>
                        <?php if (isset($Role)): ?>
                            <?php if ($Role == 1 || $Role == 2): ?>
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-pie-chart"></i>
                                    <span class="title">ใบรายงาน</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start ">
                                        <a href="../Report/RentReport.php" class="nav-link ">
                                            <i class=""></i>
                                            <span class="title">รายงานการเช่ารถ</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="../Report/Repair.php" class="nav-link ">
                                            <i class=""></i>
                                            <span class="title">รายงานค่าซ่อมรถ</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="../Report/Employee.php" class="nav-link ">
                                            <i class=""></i>
                                            <span class="title">รายงานสถานะพนักงานขับรถ</span>
                                        </a>
                                    </li>
                                </ul>
                            <?php endif ?>   
                        <?php endif ?>
                    </li>
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
            <!-- END SIDEBAR -->
        </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->

       
