<!DOCTYPE html>
<html>
<head>
    <title>Login : Smart-Coop</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <style type="text/css">
        @media only screen and (max-width : 895px) {
            #imagepttpm {
                display: none;
            }
        }

        #imagepttpm {
            background-image: url(../../assets/img/bg.jpg);
            background-size: cover;
            height: 100vh;

        }

        .Copyright {
            position: fixed;
            bottom: 0;
            text-align: center;
            margin-bottom: 1%;
            margin-left: 2%;
        }

        .headPtt {
            top: 0;
            text-align: center;
        }

        /*#imagepttpm .table tr {
            height:8px;
        }*/

        #imagepttpm .table > tbody > tr > td {
            padding: 2px;
        }

    </style>
    <script type="text/javascript">
        $(function () {
            $('#Login').on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: '../../PHP/Authen.php',
                    data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false, 
                    success: function (response) {
                        
                        if(response.status == 'success'){
                           window.location.href='./Home.php';
                        }else if(response.status == 'error'){
                            $('#validateLogin').css('display','block');
                        }    
                    }
                    
                });
            
            });
        });
    </script>
</head>
<body>
<div class="col-md-12" style="padding: 0px">
    <div class="col-md-8" id="imagepttpm" style="padding-left:0px !important;padding-right:0px !important">
        <div style="background-color: rgba(1, 1, 1, 0.27) ; height: 100%; width: 100%; "></div>
    </div>
    <div class="col-md-4">
        <div class="col-md-12" style="margin-top: 5%">
            <div class="col-md-12">
                <span class="headPtt">
                    <!--PTT POLYMER MARKETING-->
                </span>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 40%; ">

            <div>
                <div class="col-md-2 col-md-offset-2">
                    <!-- <img src="../../assets/images/logo/siamlogo.png" class="img-rounded" width="auto" height="70"> -->
                </div>
                <div class="col-md-8" style="padding-top:4%"><span style="color: #019fa8; font-size: 16px; font-weight: 600; vertical-align: bottom; text-align: left;">Rent System</span></div>
            </div>


        </div>
        <div id="validateLogin" class="col-md-12" style="height:20px;display: none;">
            <span class="label label-danger col-md-5 col-md-offset-4" data-bind="visible:IsShowInvalid">
                Username or Password invalid
            </span>
        </div>
        <div class="col-md-12" style="text-align: center; margin-top: 4%;">
            <form class="form-horizontal" id="Login">
                <div class="col-md-12">
                    <div class="col-md-2"><label class="control-label text-left"><span style="color: #a3c7e4;font-size: 22px" class="glyphicon glyphicon-user" aria-hidden="true"></span></label></div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" name="Username" id="Username" class="form-control" placeholder="Username">
                        </div>
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><label class="control-label text-left"><span style="color: #a3c7e4;font-size: 22px" class="glyphicon glyphicon-lock" aria-hidden="true"></label></div>
                    <div class="col-md-8">
                        <div class="form-group">
                             <input type="password" name="Password" class="form-control" placeholder="Password">
                        </div>
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input style="width: 100%;background-color: #c4efbe;color:white;font-weight:800" class="btn blue" type="submit" value="Login" name="">
                        </div>
                    </div>

                </div>
            </form>
            <div class="col-md-12">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="form-group">
                        <!--<a data-bind="attr:{href:'./ForgotPassword.aspx'}">Forgot your Password ?</a>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <span class="Copyright">
                <!--Copyright © 2016 PTT Polymer Marketing Company Limited. <br> All Right Reserved-->
            </span>
        </div>
    </div>
</div>
</body>
</html>
