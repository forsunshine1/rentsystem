<?php 
    include '../_Master/_header.php';
    if(isset($Role)){
        
    }else{
        echo "<script type='text/javascript'>window.location.href = '../Login/Login.php';</script>";  
    }
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Id'])) {

        $Id = $_GET['Id'];
        $query = mysqli_query($con,"SELECT * FROM Employee WHERE Id = '$Id' ");
        
        // $result = mysqli_query($con,$sql); 
        $row=mysqli_fetch_array($query,MYSQLI_ASSOC);
        
    }
?>
<!-- POST  -->
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
           type: 'post',
                url: './Controller.php',
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,
                
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", "./Search.php");
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }                
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "แก้ไขประวัติ"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">ข้อมูลพนักงานขับรถ</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>แก้ไขประวัติ </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="#" class="form-horizontal" id="addForm">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">ชื่อ <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <input type="text" id="Firstname" name="Firstname" class="form-control" placeholder="" value="<?php if(isset($_GET['Id']) != ''){echo $row['Firstname']; }?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">นามสกุล</label>
                                    <div class="col-md-4">
                                        <input type="text" id="Lastname" name="Lastname" class="form-control" placeholder="" value="<?php if(isset($_GET['Id']) != ''){echo $row['Lastname']; }?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">เลขบัตรประชาชน <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <input type="text" id="IdCard" name="IdCard" class="form-control" placeholder="" value="<?php if(isset($_GET['Id']) != ''){echo $row['IdCard']; }?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">ที่อยู่</label>
                                    <div class="col-md-4">
                                        <input type="text" id="Address" name="Address" class="form-control" placeholder="" value="<?php if(isset($_GET['Id']) != ''){echo $row['Address']; }?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">เบอร์โทรศัพท์ <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <input type="text" id="Tel" name="Tel" class="form-control" placeholder="" value="<?php if(isset($_GET['Id']) != ''){echo $row['Tel']; }?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Avatar</label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if ($row['Img'] != ''): ?>
                                                    <img src="../../avatar/<?php echo $row['Img']; ?>" alt="" />
                                                <?php endif ?>
                                                <?php if ($row['Img'] == ''): ?>
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                <?php endif ?>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> เลือกรูป </span>
                                                    <span class="fileinput-exists"> เปลี่ยน </span>
                                                    <input type="file" name="img_member"> 
                                                </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> ลบ 
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="Avatar" class="form-control" value="<?php echo $row['Img'] ?>" required>
                                <input type="hidden" name="func" class="form-control" value="Edit">
                                <input type="hidden" value="<?php echo $row['Id'] ?>" name="Id">
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn green"><i class="fa fa-check"></i> ตกลง</button>
                                        <a href="./Search.php" class="btn default">ยกเลิก</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END EXTRAS PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>