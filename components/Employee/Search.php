<?php 
    include '../_Master/_header.php';
    if(isset($Role)){
       
    }else{
        echo "<script type='text/javascript'>window.location.href = '../Login/Login.php';</script>";  
    }
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Employee']) != '') {
    // $sql = "SELECT * FROM faculty WHERE FacultyCode LIKE '%" . $_GET['FacultyCode'] . "%' "OR" FacultyName LIKE '%" . $_GET['FacultyName'] . "%' "; 
        if (isset($_GET['Employee']) != "") {
            $Employee = "Firstname LIKE'%" . $_GET['Employee'] . "%'";
        }else{
            $Employee = "";
        }
        if (isset($_GET['Employee']) != "") {
            $Lastname = "Lastname LIKE'%" . $_GET['Employee'] . "%'";
        }else{
            $Lastname = "";
        }

        $sql = "SELECT * FROM employee WHERE $Employee or $Lastname order by firstname ASC";

        $result = mysqli_query($con,$sql); 
    }else{
        $sql = "SELECT * FROM employee";  
        $result = mysqli_query($con,$sql); 
    }   
    mysqli_close($con);
?>
<!-- POST  -->

<script type="text/javascript">
    document.title = "ข้อมูลพนักงานขับรถ"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">ข้อมูลพนักงานขับรถ</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="portlet light bordered" id="addPanel" >
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-search font-red-sunglo"></i>
                                    <span class="caption-subject font-red-sunglo bold uppercase">ค้นหา</span>
                                </div>
                            <div class="actions">
                                <a class="btn yellow" href="Create.php">สร้างข้อมูล</a>
                            </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal" id="addForm">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">รายชื่อพนักงาน</label>
                                            <div class="col-md-4">
                                                <input type="text" name="Employee" class="form-control" placeholder="ชื่อ">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn blue">ค้นหา</button>
                                            </div>
                                        </div>
                                    </div>                                         
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="fa fa-table font-red-sunglo"></i>
                            <span class="caption-subject font-red-sunglo bold uppercase">ผลการค้นหา</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>ชื่อ</th>                                    
                                    <th>นามสกุล</th>
                                    <th>เลขบัตรประชาชน</th>
                                    <th>ที่อยู่</th>
                                    <th>เบอร์โทรศัพท์</th>
                                    <th>วันที่สร้าง</th>
                                    <th>สถานะ</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $i = 1;
                                    while ($row = mysqli_fetch_assoc($result)) {  
                                ?>
                               <tr>  
                                    <td><?php echo $row["Firstname"]; ?></td>  
                                    <td><?php echo $row["Lastname"]; ?></td>
                                    <td><?php echo $row["IdCard"]; ?></td>
                                    <td><?php echo $row["Address"]; ?></td>
                                    <td><?php echo $row["Tel"]; ?></td>  

                                    <td><?php echo date('d/m/Y', strtotime($row['Created']));?></td>  
                                    <?php if ($row["Status"] == 0): ?>
                                             <td>ปกติ</td>
                                    <?php endif ?> 
                                    <?php if ($row["Status"] == 1): ?>
                                            <td style="color:red">บัญชีดำ</td>
                                    <?php endif ?>    
                                    <td >
                                        <?php if ($row["Status"] == 1): ?>
                                            <?php echo '<a class="btn-xs btn  green" data-toggle="modal" data-id="'. $row['Id'] .'" href="#myModalStatus2'. $row['Id'] .'"  >เปลี่ยนสถานะ</a>'; ?>
                                        <?php endif ?>
                                        <?php if ($row["Status"] == 0): ?>
                                            <?php echo '<a class="btn-xs btn red" data-toggle="modal" data-id="'. $row['Id'] .'" href="#myModalStatus'. $row['Id'] .'"  >บัญชีดำ</a>'; ?>
                                        <?php endif ?>
                                        <?php echo '<a class="btn-xs btn  green"  href="Edit.php?Id='. $row['Id'] .'" >แก้ไข</a>'; ?>
                                        <?php echo '<a class="btn-xs btn  red" data-toggle="modal" data-id="'. $row['Id'] .'" href="#myModal'. $row['Id'] .'"  >ลบ</a>'; ?>
                                    </td>                           
                                    <!-- Modal Delete-->
                                    <div class="modal fade bs-example-modal-sm" id="myModal<?php echo $row['Id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header" style="background-color: #f9243f;">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 style="color:#fff;" class="modal-title" id="myModalLabel">Alert</h4>
                                            </div>
                                            <div class="modal-body">
                                              ลบข้อมูล <?php echo $row['Firstname'] ; ?>
                                            </div>
                                            <div class="modal-footer">
                                              <a class="btn red" href="./Controller.php?Id=<?php echo $row['Id']?>&func=Delete">ตกลง</a>
                                              <a class="btn default" data-dismiss="modal">ยกเลิก</a>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- Modal Delete-->
                                    </div>
                                    <!-- Modal Delete-->   
                                    <div class="modal fade bs-example-modal-sm" id="myModalStatus<?php echo $row['Id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header" style="background-color: #f9243f;">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 style="color:#fff;" class="modal-title" id="myModalLabel">Alert</h4>
                                            </div>
                                            <div class="modal-body">
                                              ยืนยันการแก้ไขสถานะ
                                            </div>
                                            <div class="modal-footer">
                                              <a class="btn red" href="./Controller.php?Id=<?php echo $row['Id']?>&func=Status&Status=<?php echo $row['Status']?>">ตกลง</a>
                                              <a class="btn default" data-dismiss="modal">ยกเลิก</a>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- Modal Delete-->
                                    </div>
                                    <!-- Modal Delete-->  
                                    <div class="modal fade bs-example-modal-sm" id="myModalStatus2<?php echo $row['Id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header" style="background-color: #f9243f;">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 style="color:#fff;" class="modal-title" id="myModalLabel">Alert</h4>
                                            </div>
                                            <div class="modal-body">
                                              ยืนยันการแก้ไขสถานะ
                                            </div>
                                            <div class="modal-footer">
                                              <a class="btn red" href="./Controller.php?Id=<?php echo $row['Id']?>&func=Status&Status=<?php echo $row['Status']?>">ตกลง</a>
                                              <a class="btn default" data-dismiss="modal">ยกเลิก</a>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- Modal Delete-->
                                    </div>
                                    <!-- Modal Delete-->                      
                                </tr>
                                <?php  
                                    };  
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>