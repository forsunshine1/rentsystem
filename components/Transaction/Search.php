<?php 
    include '../_Master/_header.php';
     if(isset($Role)){

    }else{
        echo "<script type='text/javascript'>window.location.href = '../Login/Login.php';</script>";  
    }
    include '../../PHP/ConnectDB.php';
    $sql = "SELECT 
                tc.Id,
                tc.RentDate ,
                tc.ReturnDate,
                tc.StatusCar,
                tc.StatusPayment,
                tc.IsClosed,
                em.Firstname,
                em.Lastname,
                em.Tel,
                ct.Cost,
                ct.Description,
                car.CarId As CarNo,
                car.PathImg1,
                ct.Cost
            FROM transaction as tc
            Left JOIN employee as em on tc.EmployeeId = em.Id
            left join cost as ct on tc.CostId = ct.Id
            left join carinformation as car on tc.CarId = car.Id
            ";  
    $result = mysqli_query($con,$sql); 
    mysqli_close($con);
?>
<!-- POST  -->

<script type="text/javascript">
    document.title = "ข้อมูลพนักงานขับรถ"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">ข้อมูลพนักงานขับรถ</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="portlet light bordered" id="addPanel" >
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-search font-red-sunglo"></i>
                                    <span class="caption-subject font-red-sunglo bold uppercase">ค้นหา</span>
                                </div>
                            <div class="actions">
                                <a class="btn yellow" href="Create.php">สร้างข้อมูล</a>
                            </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal" id="addForm">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">ชื่อ</label>
                                            <div class="col-md-4">
                                                <input type="text" name="Employee" class="form-control" placeholder="ชื่อ">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">วันที่เช่า</label>
                                            <div class="col-md-4">
                                                <input type="date" name="Employee" class="form-control" placeholder="ชื่อ">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">วันทีคืน</label>
                                            <div class="col-md-4">
                                                <input type="date" name="Employee" class="form-control" placeholder="ชื่อ">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="single"  class="col-md-3 control-label text-right" style="padding-top: 1%">สถานะรถ</label>
                                            <div class="col-md-4">
                                                <select id="single" class="form-control select2" name="StatusId">
                                                    <option value='-1'>กรุณาเลือก</option>
                                                    <option value='0'>คืน</option>
                                                    <option value='1'>ยังไม่คืน</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="single"  class="col-md-3 control-label text-right" style="padding-top: 1%">สถานะการเงิน</label>
                                            <div class="col-md-4">
                                                <select id="single" class="form-control select2" name="StatusId">
                                                    <option value='-1'>กรุณาเลือก</option>
                                                    <option value='0'>ชำระ</option>
                                                    <option value='1'>ยังชำระ</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="single"  class="col-md-3 control-label text-right" style="padding-top: 1%">สถานะ</label>
                                            <div class="col-md-4">
                                                <select id="single" class="form-control select2" name="StatusId">
                                                    <option value='-1'>กรุณาเลือก</option>
                                                    <option value='0'>ปิด</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn blue">ค้นหา</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>                                           
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="fa fa-table font-red-sunglo"></i>
                            <span class="caption-subject font-red-sunglo bold uppercase">ผลการค้นหา</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                       <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>ชื่อ</th>
                                    <th>ทะเบียนรถ</th>   
                                    <th>ค่าเช่า</th>                                 
                                    <th>วันที่เช่า</th>
                                    <th>วันที่คืน</th>
                                    <th>สถานะรถ</th>
                                    <th>สถานะการเงิน</th>
                                    <th>สถานะการยืม</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $i = 1;
                                    while ($row = mysqli_fetch_assoc($result)) {  
                                ?>
                               <tr> 
                                    <td><?php echo $row["Firstname"]; ?> - <?php echo $row["Lastname"]; ?></td>  
                                    <td><?php echo $row["CarNo"]; ?></td>
                                    <td><?php echo number_format($row["Cost"], 2, '.', ''); ?></td>  
                                    <td><?php echo date('d/m/Y', strtotime($row['RentDate'])); ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($row['ReturnDate'])); ?></td>
                                    <td><?php echo $row['StatusCar'] == 0 ? 'คืน':'ยังไม่คืน'; ?></td>
                                    <td><?php echo $row['StatusPayment'] == 0 ? 'ชำระเงิน' : 'ยังชำระเงิน'; ?></td>
                                    <td><?php echo $row['IsClosed'] == NULL || $row['IsClosed'] == ''? ' ' : 'ปิด'; ?></td>
                                    <td>
                                        <?php echo '<a class="btn-xs btn green" data-toggle="modal" data-id="'. $row['Id'] .'" href="#myModalStatus'. $row['Id'] .'"  >แก้ไขสถานะ</a>'; ?>
                                        <?php echo '<a class="btn-xs btn  red" data-toggle="modal" data-id="'. $row['Id'] .'" href="#myModal'. $row['Id'] .'"  >ลบ</a>'; ?>
                                    </td>                           
                                    <!-- Modal Delete-->
                                    <div class="modal fade bs-example-modal-sm" id="myModal<?php echo $row['Id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header" style="background-color: #f9243f;">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 style="color:#fff;" class="modal-title" id="myModalLabel">Alert</h4>
                                            </div>
                                            <div class="modal-body">
                                              ลบข้อมูล
                                            </div>
                                            <div class="modal-footer">
                                              <a class="btn red" href="./Controller.php?Id=<?php echo $row['Id']?>&func=Delete">ตกลง</a>
                                              <a class="btn default" data-dismiss="modal">ยกเลิก</a>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- Modal Delete-->
                                    </div>
                                    <!-- Modal Delete-->     
                                    <div class="modal fade" id="myModalStatus<?php echo $row['Id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header" style="background-color: #32c5d2;">
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                  <h4 style="color:#fff;" class="modal-title" id="myModalLabel">สถานะ</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form class="form-horizontal" id="updateAdviser<?php echo $row['Id'] ?>">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label for="single"  class="col-md-3 control-label text-right" style="padding-top: 1%">สถานะ</label>
                                                                <div class="col-md-8">
                                                                    <select id="single" class="form-control select2" name="StatusId">
                                                                        <option value='-1'>กรุณาเลือก</option>
                                                                        <option value='1'>ปิด</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" value="<?php echo $row['Id'] ?>" name="TransactionId">
                                                            <input type="hidden" name="func" class="form-control" value="UpdateStatus">
                                                            <div class="form-group text-center" style="padding-top: 10%">
                                                                <button type="submit" class="btn green">บันทึก</button>
                                                                <a class="btn default" data-dismiss="modal">ยกเลิก</a>
                                                            </div>
                                                        </div> 
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal Delete-->
                                    </div>
                                    <!-- Modal Delete--> 
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#updateAdviser<?php echo $row['Id'] ?>').on('submit', function (e) {
                                                e.preventDefault();
                                                $.ajax({
                                                    type: 'post',
                                                    url: './Controller.php',
                                                    data: $(this).serialize(),
                                                    success: function (response) {
                                                        if(response.status == 'success'){
                                                            document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                                                            $('#Link').attr("data-dismiss",""); 
                                                            $('#Link').attr("href", "./Search.php");
                                                            $('#myModalAlert').modal('show');                
                                                        }else if(response.status == 'error'){
                                                            document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                                                            $('#myModalAlert').modal('show'); 
                                                            $('#Link').attr("data-dismiss","modal");                           
                                                        }else if(response.status == 'duplicate'){
                                                            document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                                                            $('#myModalAlert').modal('show');
                                                            $('#Link').attr("data-dismiss","modal");                      
                                                        }         
                                                    }
                                                    
                                                });
                                            
                                            });
                                        });
                                    </script>                                    
                                </tr>
                                <?php  
                                    };  
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>