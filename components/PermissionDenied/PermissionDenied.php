<?php include '../_Master/_header.php'; ?>
<script type="text/javascript">
    document.title = "Permission Denied"
</script>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">ประเภทยี่ห้อรถ</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                     <div class="col-md-12 page-500">
                            <br/> </p>
                            <div class=" number font-red"> Permission Denied </div>
                            <div class="row">
                                <div class="details">
                                    <p>
                                        <a href="../Login/Home.php" class="btn red btn-outline"> Return home </a>
                                        <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<?php include '../_Master/_footer.php'; ?>