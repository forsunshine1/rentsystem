<?php 
    include '../_Master/_header.php';
    if(isset($Role)){
        if ($Role != 0) {
            echo "<script type='text/javascript'>window.location.href = '../PermissionDenied/PermissionDenied.php';</script>";  
        }
    }else{
        echo "<script type='text/javascript'>window.location.href = '../Login/Login.php';</script>";  
    }

    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Name']) != '' || isset($_GET['Role']) != '') {
    // $sql = "SELECT * FROM faculty WHERE FacultyCode LIKE '%" . $_GET['FacultyCode'] . "%' "OR" FacultyName LIKE '%" . $_GET['FacultyName'] . "%' "; 
        if (isset($_GET['Name']) != "") {
            $Name = "Username LIKE'%" . $_GET['Name'] . "%'";
        }else{
            $Name = "";
        }
        if (trim($_GET['Role']) != "" AND $_GET['Role'] != '-1') {
            $Role = "Role LIKE'%" . $_GET['Role'] . "%'";
        }else{
            $Role = "Role IS NOT NULL";
        }

        $sql = "SELECT * FROM user WHERE $Name AND $Role";

        $result = mysqli_query($con,$sql); 
    }else{
        $sql = "SELECT * FROM user";  
        $result = mysqli_query($con,$sql); 
    }   

    $result = mysqli_query($con,$sql); 
    mysqli_close($con);
?>
<!-- POST  -->
<script type="text/javascript">
    document.title = "พนักงาน"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">พนักงาน</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="portlet light bordered" id="addPanel" >
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-search font-red-sunglo"></i>
                                    <span class="caption-subject font-red-sunglo bold uppercase">ค้นหา</span>
                                </div>
                            <div class="actions">
                                <a class="btn yellow" href="Create.php">สร้างข้อมูล</a>
                            </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal" id="addForm">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">ชื่อ</label>
                                            <div class="col-md-4">
                                                <input type="text" name="Name" class="form-control" placeholder="ชื่อ">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">บทบาท</label>
                                            <div class="col-md-4">
                                                 <select id="single" class="form-control select2" name="Role">
                                                   <option value='-1'>ทั้งหมด</option>
                                                   <option value="0">ผู้ดูแลระบบ</option>
                                                   <option value="1">เจ้าของ</option>
                                                   <option value="2">พนักงาน</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn blue">ค้นหา</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                           
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="fa fa-table font-red-sunglo"></i>
                            <span class="caption-subject font-red-sunglo bold uppercase">ผลการค้นหา</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> ชื่อ </th>
                                    <th> บทบาท </th>
                                    <th> สร้างวันที่ </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $i = 1;
                                    while ($row = mysqli_fetch_assoc($result)) {  
                                ?>
                               <tr>  
                                    <td><?php echo $row["Username"]; ?></td>  
                                    <td>
                                        <?php if ($row["Role"] == 0): ?>
                                            ผู้ดูแลระบบ
                                        <?php endif ?>
                                        <?php if ($row["Role"] == 1): ?>
                                            เจ้าของ
                                        <?php endif ?>
                                        <?php if ($row["Role"] == 2): ?>
                                            พนักงาน
                                        <?php endif ?>
                                    </td> 
                                    <td><?php echo date('d/m/Y', strtotime($row['Created']));?></td>  
                                    <td>
                                        <?php echo '<a class="btn-xs btn  green"  href="Edit.php?Id='. $row['Id'] .'" >แก้ไข</a>'; ?>
                                        <?php echo '<a class="btn-xs btn  red" data-toggle="modal" data-id="'. $row['Id'] .'" href="#myModal'. $row['Id'] .'"  >ลบ</a></td>'; ?>
                                    </td>                           
                                    <!-- Modal Delete-->
                                    <div class="modal fade bs-example-modal-sm" id="myModal<?php echo $row['Id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header" style="background-color: #f9243f;">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 style="color:#fff;" class="modal-title" id="myModalLabel">Alert</h4>
                                            </div>
                                            <div class="modal-body">
                                              ลบข้อมูล <?php echo $row['Username']; ?>
                                            </div>
                                            <div class="modal-footer">
                                              <a class="btn red" href="./Controller.php?Id=<?php echo $row['Id']?>&func=Delete">ตกลง</a>
                                              <a class="btn default" data-dismiss="modal">ยกเลิก</a>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- Modal Delete-->
                                    </div>
                                    <!-- Modal Delete-->                     
                                </tr>
                                <?php  
                                    };  
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>