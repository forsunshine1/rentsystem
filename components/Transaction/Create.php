<?php 
    include '../_Master/_header.php';
     if(isset($Role)){

    }else{
        echo "<script type='text/javascript'>window.location.href = '../Login/Login.php';</script>";  
    }
?>
<!-- POST  -->
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", "./Search.php");
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }                
                }
                
            });
        
        });
    });

    function selectCategory(CategoryId) {
        if (CategoryId != "-1") {
            loadRange('range',CategoryId);
            $("#term_dropdown").html("<option value='-1'>");
        }
    };
    function loadRange(loadType,CategoryId) {
        var dataString ='loadType='+loadType+'&loadId='+CategoryId;
        $.ajax({
            type: "POST",
            url:'./load_Range.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#"+loadType+"_loader").hide();
                $("#"+loadType+"_dropdown").html("<option value='-1'>Select "+ loadType +"</option>");
                $("#"+loadType+"_dropdown").append(result);
            }
        });
    }
    function selectRange(CostId) {
        var dataString ='CostId='+CostId;
        $.ajax({
            type: "POST",
            url:'./load_cost.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#Cost").val(result);
            }
        });
        
        var dataString ='CostId='+CostId;
        $.ajax({
            type: "POST",
            url:'./load_rentdate.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#ReturnDate").val(result);
            }
        });
        var dataString ='CostId='+CostId;
        $.ajax({
            type: "POST",
            url:'./load_rentdate2.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#RentDate").val(result);
            }
        });
    };
</script>
<script type="text/javascript">
    document.title = "เพิ่มข้อมูลพนักงานขับรถ"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Transaction</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Transaction </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="#" class="form-horizontal" id="addForm">
                            <div class="form-body">                               
                                <?php 
                                    include '../../PHP/ConnectDB.php';

                                    $sql = "SELECT * FROM employee order by Firstname ASC ";  
                                    $listemployee = mysqli_query($con,$sql); 
                                    
                                    mysqli_close($con);
                                ?>
                                <div class="form-group">
                                    <label for="single"  class="col-md-3 control-label">ลูกค้า <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <select id="single" class="form-control select2" name="EmployeeId">
                                            <?php while ($row2=mysqli_fetch_assoc($listemployee)) { ?>
                                                <option value=<?php echo $row2['Id']?>><?php echo $row2['Firstname']?> - <?php echo $row2['Lastname']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <?php 
                                    include '../../PHP/ConnectDB.php';

                                    $sql = "SELECT * FROM carinformation order by CarId ASC ";  
                                    $listcar = mysqli_query($con,$sql); 
                                    
                                    mysqli_close($con);
                                ?>
                                <div class="form-group">
                                    <label for="single"  class="col-md-3 control-label">รถ <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <select id="single" class="form-control select2" name="CarId">
                                            <?php while ($row3=mysqli_fetch_assoc($listcar)) { ?>
                                                <option value=<?php echo $row3['Id']?>><?php echo $row3['CarId']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <?php 
                                    include '../../PHP/ConnectDB.php';

                                    $sql = "SELECT * FROM category ";  
                                    $listterm = mysqli_query($con,$sql); 
                                    
                                    mysqli_close($con);
                                ?>
                                <div class="form-group">
                                    <label for="single"  class="col-md-3 control-label">ประเภทรถ <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <select onchange="selectCategory(this.options[this.selectedIndex].value)" id="single" class="form-control select2" name="CategoryId">
                                            <?php if ($row['YearId'] != ''): ?>
                                                <option value='<?php echo $row['CategoryId']?>'><?php echo $row['Type']?></option>
                                            <?php endif ?>
                                            <?php if ($row['YearId'] == ''): ?>
                                                <option value="-1">Select Type</option>
                                            <?php endif ?>
                                            <?php while ($row2=mysqli_fetch_assoc($listterm)) { ?>
                                                <option value="<?php echo $row2['Id']?>"><?php echo $row2['Type']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="single"  class="col-md-3 control-label">ช่วงระยะเวลา <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <select onchange="selectRange(this.options[this.selectedIndex].value)" id="range_dropdown" class="form-control select2" name="CostId">
                                            <?php if ($row['RangeId'] != ''): ?>
                                                <option value='<?php echo $row['RangeId']?>'><?php echo $row['RangeName']?></option>      
                                            <?php endif ?>  
                                            <?php if ($row['TermId'] == ''): ?>
                                                <option value="-1">Select Range</option>
                                            <?php endif ?>
                                            
                                            <span id="range_loader"></span>
                                        </select>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-md-3 control-label">ค่าเช่า</label>
                                    <div class="col-md-4">
                                        <input type="text" id="Cost" class="form-control" placeholder="" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="single"  class="col-md-3 control-label">Rent Date</label>
                                    <div class="col-md-4">
                                        <input id='RentDate' type="text" class="form-control" name="RentDate" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="single"  class="col-md-3 control-label">Return Date</label>
                                    <div class="col-md-4">
                                        <input id='ReturnDate' type="text" class="form-control" name="ReturnDate" readonly>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label for="single"  class="col-md-3 control-label">จ่าย <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <select id="single" class="form-control select2" name="StatusPayment">
                                            <option value="0">ชำระเงิน</option>
                                            <option value="1">ยังไม่ชำระเงิน</option>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="func" value="Create">
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn green"><i class="fa fa-check"></i> ตกลง</button>
                                        <a href="./Search.php" class="btn default">ยกเลิก</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END EXTRAS PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>