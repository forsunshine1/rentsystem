<?php 
    include '../_Master/_header.php';
     if(isset($Role)){
         if ($Role == 0) {
            echo "<script type='text/javascript'>window.location.href = '../PermissionDenied/PermissionDenied.php';</script>";  
        }
    }else{
        echo "<script type='text/javascript'>window.location.href = '../Login/Login.php';</script>";  
    }
?>
<!-- POST  -->

<script type="text/javascript">
    document.title = "รายงานสถานะพนักงานขับรถ"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">รายงานสถานะพนักงานขับรถ</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="portlet light bordered" id="addPanel" >
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-search font-red-sunglo"></i>
                                    <span class="caption-subject font-red-sunglo bold uppercase">ค้นหา</span>
                                </div>
                            <div class="actions">
                            </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal" id="addForm">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">ชื่อ</label>
                                            <div class="col-md-4">
                                                <input type="text" name="Employee" class="form-control" placeholder="ชื่อ">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="single"  class="col-md-3 control-label text-right" style="padding-top: 1%">สถานะ</label>
                                            <div class="col-md-4">
                                                <select id="single" class="form-control select2" name="StatusId">
                                                    <option value='-1'>กรุณาเลือก</option>
                                                    <option value='0'>ปกติ</option>
                                                    <option value='1'>บัญชีดำ</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                   <a class="btn blue" href="../../Reports/EmployeeReport.php">Report</a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>                                           
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>