<?php 
    include '../_Master/_header.php';
    include '../../PHP/ConnectDB.php';     
    if (isset($_GET['Carinformation']) != '') {
        // $sql = "SELECT * FROM faculty WHERE FacultyCode LIKE '%" . $_GET['FacultyCode'] . "%' "OR" FacultyName LIKE '%" . $_GET['FacultyName'] . "%' "; 
            if (isset($_GET['Carinformation']) != "") {
                $Brand = "cr.CarId LIKE'%" . $_GET['Carinformation'] . "%'";
            }else{
                $Brand = "";
            }

            $sql = "SELECT cr.Status,cr.IsRent,cr.Id , cr.CarId , cr.ModelCarId , model.Name as ModelName ,cr.Created FROM carinformation as cr left join model on cr.ModelCarId = model.Id WHERE $Brand";

            $result = mysqli_query($con,$sql); 
        }else{
            $sql = "SELECT cr.Status,cr.IsRent,cr.Id , cr.CarId , cr.ModelCarId , model.Name as ModelName ,cr.Created FROM carinformation as cr left join model on cr.ModelCarId = model.Id ";  
            $result = mysqli_query($con,$sql);
        }
        mysqli_close($con);

?>
<!-- POST  -->

<script type="text/javascript">
    document.title = "ข้อมูลรถ"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">ข้อมูลรถ</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="portlet light bordered" id="addPanel" >
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-search font-red-sunglo"></i>
                                    <span class="caption-subject font-red-sunglo bold uppercase">ค้นหา</span>
                                </div>
                            <div class="actions">
                                <a class="btn yellow" href="Create.php">สร้างข้อมูล</a>
                            </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal" id="addForm">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">เลขทะเบียน</label>
                                            <div class="col-md-4">
                                                <input type="text" name="Carinformation" class="form-control" placeholder="เลขทะเบียน">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn blue">ค้นหา</button>
                                            </div>
                                        </div>
                                    </div>                                         
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="page-content-inner">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="fa fa-table font-red-sunglo"></i>
                            <span class="caption-subject font-red-sunglo bold uppercase">ผลการค้นหา</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> เลขทะเบียน </th>
                                    <th> รุ่น </th>
                                    <th> สร้างวันที่ </th>
                                    <th>สภาพรถ</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $i = 1;
                                    while ($row = mysqli_fetch_assoc($result)) {  
                                ?>    
                               <tr>  
                                    <td><?php echo $row["CarId"]; ?></td>  
                                    <td><?php echo $row["ModelName"]; ?></td> 
                                    <td><?php echo date('d/m/Y', strtotime($row['Created']));?></td>  
                                    <th>
                                        <?php if ($row['Status'] == 0): ?>
                                            <?php echo "ปกติ"; ?>
                                        <?php endif ?>
                                        <?php if ($row['Status'] == 1): ?>
                                            <?php echo "ส่งซ่อม"; ?>
                                        <?php endif ?>
                                        <?php if ($row['Status'] == 2): ?>
                                            <?php echo "ชำรุด"; ?>
                                        <?php endif ?>
                                    </th>
                                    <th>
                                        <?php if ($row['IsRent'] == 0): ?>
                                            <?php echo "อยู่"; ?>
                                        <?php endif ?>
                                        <?php if ($row['IsRent'] == 1): ?>
                                            <?php echo "ไม่อยู่"; ?>
                                        <?php endif ?>
                                    </th>         
                                    <td>
                                        <?php echo '<a class="btn-xs btn  red" data-toggle="modal" data-id="'. $row['Id'] .'" href="#myModalStatus'. $row['Id'] .'"  >แก้ไขสถานะ</a>'; ?>
                                        <?php echo '<a class="btn-xs btn  green"  href="Edit.php?Id='. $row['Id'] .'" >แก้ไข</a>'; ?>
                                        <?php echo '<a class="btn-xs btn  red" data-toggle="modal" data-id="'. $row['Id'] .'" href="#myModal'. $row['Id'] .'"  >ลบ</a>'; ?>
                                    </td>
                                                      
                                    <!-- Modal Delete-->
                                    <div class="modal fade bs-example-modal-sm" id="myModal<?php echo $row['Id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header" style="background-color: #f9243f;">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 style="color:#fff;" class="modal-title" id="myModalLabel">Alert</h4>
                                            </div>
                                            <div class="modal-body">
                                              ลบข้อมูล <?php echo $row['CarId']?>
                                            </div>
                                            <div class="modal-footer">
                                              <a class="btn red" href="./Controller.php?Id=<?php echo $row['Id']?>&func=Delete">ตกลง</a>
                                              <a class="btn default" data-dismiss="modal">ยกเลิก</a>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- Modal Delete-->
                                    </div>
                                    <!-- Modal Delete--> 
                                    <div class="modal fade bs-example-modal-sm" id="myModalStatus<?php echo $row['Id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title" id="myModalLabel">แก้ไขสถานะ</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" id="UpdateStatus<?php echo $row['Id'] ?>">
                                                    <div class="form-body">
                                                        <select id="single" class="form-control select2" name="Status">
                                                           <option value="0">ปกติ</option>
                                                           <option value="1">ส่งซ่อม</option>
                                                           <option value="2">ชำรุด </option>
                                                        </select>
                                                        <input type="hidden" value="<?php echo $row['Id'] ?>" name="CarId">
                                                        <input type="hidden" name="func" class="form-control" value="UpdateStatus">
                                                        <div class="form-group text-center" style="padding-top: 10%">
                                                            <button type="submit" class="btn green">Save</button>
                                                            <a class="btn default" data-dismiss="modal">Cancel</a>
                                                        </div>
                                                    </div> 
                                                </form>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- Modal Delete-->
                                    </div>
                                    <!-- Modal Delete-->  
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#UpdateStatus<?php echo $row['Id'] ?>').on('submit', function (e) {
                                                e.preventDefault();
                                                $.ajax({
                                                    type: 'post',
                                                    url: './Controller.php',
                                                    data: $(this).serialize(),
                                                    success: function (response) {
                                                        if(response.status == 'success'){
                                                            document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                                                            $('#Link').attr("data-dismiss",""); 
                                                            $('#Link').attr("href", "./Search.php");
                                                            $('#myModalAlert').modal('show');                
                                                        }else if(response.status == 'error'){
                                                            document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                                                            $('#myModalAlert').modal('show'); 
                                                            $('#Link').attr("data-dismiss","modal");                           
                                                        }else if(response.status == 'duplicate'){
                                                            document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                                                            $('#myModalAlert').modal('show');
                                                            $('#Link').attr("data-dismiss","modal");                      
                                                        }         
                                                    }
                                                    
                                                });
                                            
                                            });
                                        });
                                    </script>                                      
                                </tr>
                                <?php  
                                    };  
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>