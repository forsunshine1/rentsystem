<?php 
    include '../_Master/_header.php';
     if(isset($Role)){
        if ($Role == 2) {
            echo "<script type='text/javascript'>window.location.href = '../PermissionDenied/PermissionDenied.php';</script>";  
        }
    }else{
        echo "<script type='text/javascript'>window.location.href = '../Login/Login.php';</script>";  
    }
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Id'])) {

        $Id = $_GET['Id'];
        $query = mysqli_query($con,"SELECT cost.Id, cost.Cost , cost.Description , cost.Created ,cost.RangeId , cost.CategoryId, rangedate.RangeName , category.Type FROM cost left join rangedate on cost.RangeId = rangedate.Id left join category on cost.CategoryId = category.Id WHERE cost.Id = '$Id' ");
        
        // $result = mysqli_query($con,$sql); 
        $row=mysqli_fetch_array($query,MYSQLI_ASSOC);
        
    }
?>
<!-- POST  -->
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", "./Search.php");
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }                
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "ค่าเช่า"
</script>

    
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="../Login/Home.php">หน้าหลัก</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">ค่าเช่า</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>กำหนดราคาเช่ารถ </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="#" class="form-horizontal" id="addForm">
                            <div class="form-body">
                                <?php 
                                    include '../../PHP/ConnectDB.php';

                                    $sql = "SELECT * FROM category  ";  
                                    $listes = mysqli_query($con,$sql); 
                                    
                                    mysqli_close($con);
                                ?>
                                <div class="form-group">
                                    <label for="single"  class="col-md-3 control-label">ประเภทรถ <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <select id="single" class="form-control select2" name="CategoryId">
                                            <option value=<?php echo $row['CategoryId']?>><?php echo $row['Type']; ?></option >
                                            <?php while ($row2=mysqli_fetch_assoc($listes)) { ?>
                                                <?php if ($row['CategoryId'] != $row2['Id']): ?>
                                                    <option value=<?php echo $row2['Id']?>><?php echo $row2['Type']?></option>
                                                <?php endif ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <?php 
                                    include '../../PHP/ConnectDB.php';

                                    $sql = "SELECT * FROM rangedate  ";  
                                    $listes = mysqli_query($con,$sql); 
                                    
                                    mysqli_close($con);
                                ?>
                                <div class="form-group">
                                    <label for="single"  class="col-md-3 control-label">ช่วงระยะเวลา <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <select id="single" class="form-control select2" name="RangeId">
                                            <option value=<?php echo $row['RangeId']?>><?php echo $row['RangeName']; ?></option >
                                            <?php while ($row2=mysqli_fetch_assoc($listes)) { ?>
                                                <?php if ($row['RangeId'] != $row2['Id']): ?>
                                                    <option value=<?php echo $row2['Id']?>><?php echo $row2['RangeName']?></option>
                                                <?php endif ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">ค่าเช่า <span class="colorRed">*</span></label>
                                    <div class="col-md-4">
                                        <input type="number" id="Cost" name="Cost" class="form-control" placeholder="Cost" value="<?php if(isset($_GET['Id']) != ''){echo number_format($row["Cost"], 2, '.', ''); }?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">รายละเอียด</label>
                                    <div class="col-md-4">
                                        <input type="text" id="Description" name="Description" class="form-control" placeholder="Description" value="<?php if(isset($_GET['Id']) != ''){echo $row['Description']; }?>">
                                    </div>
                                </div>
                                <input type="hidden" name="func" class="form-control" value="Edit">
                                <input type="hidden" value="<?php echo $row['Id'] ?>" name="Id">
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn green"><i class="fa fa-check"></i> ตกลง</button>
                                        <a href="./Search.php" class="btn default">ยกเลิก</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END EXTRAS PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<?php include '../_Master/_footer.php'; ?>