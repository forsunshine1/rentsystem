-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2018 at 08:12 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentsystemdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `Id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf16_bin NOT NULL,
  `Description` varchar(255) COLLATE utf16_bin DEFAULT NULL,
  `YearId` int(11) NOT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`Id`, `Name`, `Description`, `YearId`, `Created`) VALUES
(2, 'รถแท็กซี่sss', 'รายวัน', 1, '2018-01-06 05:42:19');

-- --------------------------------------------------------

--
-- Table structure for table `carinformation`
--

CREATE TABLE `carinformation` (
  `Id` int(11) NOT NULL,
  `CarId` varchar(255) COLLATE utf16_bin NOT NULL,
  `ModelCarId` int(11) NOT NULL,
  `PathImg1` varchar(255) COLLATE utf16_bin DEFAULT NULL,
  `PathImg2` varchar(255) COLLATE utf16_bin DEFAULT NULL,
  `PathImg3` varchar(255) COLLATE utf16_bin DEFAULT NULL,
  `PathImg4` varchar(255) COLLATE utf16_bin DEFAULT NULL,
  `PathImg5` varchar(255) COLLATE utf16_bin DEFAULT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `carinformation`
--

INSERT INTO `carinformation` (`Id`, `CarId`, `ModelCarId`, `PathImg1`, `PathImg2`, `PathImg3`, `PathImg4`, `PathImg5`, `Created`) VALUES
(2, '111f', 2, NULL, NULL, NULL, NULL, NULL, '2018-01-06 07:44:57'),
(3, 'aa', 2, NULL, NULL, NULL, NULL, NULL, '2018-01-13 06:09:17'),
(5, '00779', 2, '', '', '', '', '', '2018-01-13 06:35:43'),
(6, '007792', 2, '1515825478.jpg', '1515825478.jpg', '1515825478.jpg', '1515825478.jpg', '1515825478.jpg', '2018-01-13 06:37:58');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `Id` int(11) NOT NULL,
  `Type` varchar(255) COLLATE utf16_bin NOT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- --------------------------------------------------------

--
-- Table structure for table `cost`
--

CREATE TABLE `cost` (
  `Id` int(11) NOT NULL,
  `Cost` decimal(18,7) NOT NULL,
  `Description` varchar(255) COLLATE utf16_bin NOT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `cost`
--

INSERT INTO `cost` (`Id`, `Cost`, `Description`, `Created`) VALUES
(1, '800.0000000', '1 วัน', '2018-01-06 04:30:19'),
(2, '0.0000000', 'รายวัน', '2018-01-06 05:35:18');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `Id` int(11) NOT NULL,
  `Firstname` varchar(100) COLLATE utf16_bin NOT NULL,
  `Lastname` varchar(100) COLLATE utf16_bin NOT NULL,
  `IdCard` varchar(13) COLLATE utf16_bin NOT NULL,
  `Address` varchar(255) COLLATE utf16_bin NOT NULL,
  `Tel` varchar(20) COLLATE utf16_bin NOT NULL,
  `Img` varchar(255) COLLATE utf16_bin NOT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`Id`, `Firstname`, `Lastname`, `IdCard`, `Address`, `Tel`, `Img`, `Created`) VALUES
(1, 'ชัยชนะ', 'จารกรุง', '1029384675172', 'dssd', 'ddd', '', '2018-01-13 04:26:50'),
(2, 'sss', 'aaaaa', '12312312312', 'saddas', '444', '', '2018-01-13 04:28:59'),
(3, 'sss', 'aaaaa', '12312312312', 'saddas', '444', '', '2018-01-13 04:29:05'),
(4, 'ssddddd', 'aaaa', '111111111111', 'sd', '4124214', '1515823273.jpg', '2018-01-13 06:01:13');

-- --------------------------------------------------------

--
-- Table structure for table `model`
--

CREATE TABLE `model` (
  `Id` int(11) NOT NULL,
  `BrandId` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `model`
--

INSERT INTO `model` (`Id`, `BrandId`, `Name`, `Created`) VALUES
(2, 0, '', '2018-01-06 08:21:33');

-- --------------------------------------------------------

--
-- Table structure for table `repairinformation`
--

CREATE TABLE `repairinformation` (
  `Id` int(11) NOT NULL,
  `CarId` int(11) NOT NULL,
  `Description` varchar(255) COLLATE utf16_bin NOT NULL,
  `Cost` decimal(18,7) NOT NULL,
  `Date` datetime NOT NULL,
  `File` varchar(255) COLLATE utf16_bin DEFAULT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `repairinformation`
--

INSERT INTO `repairinformation` (`Id`, `CarId`, `Description`, `Cost`, `Date`, `File`, `Created`) VALUES
(3, 5, 'test', '2000.0000000', '2018-01-20 00:00:00', '1516432185.docx', '2018-01-20 07:09:45'),
(5, 5, '22', '22.0000000', '2018-01-17 00:00:00', NULL, '2018-01-20 07:07:43'),
(6, 5, 'asd', '2222.0000000', '2018-01-28 00:00:00', NULL, '2018-01-20 07:07:47');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `Id` int(11) NOT NULL,
  `EmployeeId` int(11) NOT NULL,
  `CarId` int(11) NOT NULL,
  `CostId` int(11) NOT NULL,
  `RentDate` datetime NOT NULL,
  `ReturnDate` datetime NOT NULL,
  `StatusCar` int(11) NOT NULL COMMENT '0=คืน,1ยังไม่คืน',
  `StatusPayment` int(11) NOT NULL COMMENT '0=จ่าย,1=ยังไม่จ่าย',
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Id` int(11) NOT NULL,
  `Username` varchar(20) COLLATE utf16_bin NOT NULL,
  `Password` varchar(20) COLLATE utf16_bin NOT NULL,
  `Role` int(11) NOT NULL COMMENT '0=admin,1=เจ้าของ,2=พนักงาน',
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Id`, `Username`, `Password`, `Role`, `Created`) VALUES
(14, 'admin', '1234', 1, '2017-12-09 07:29:05');

-- --------------------------------------------------------

--
-- Table structure for table `year`
--

CREATE TABLE `year` (
  `Id` int(11) NOT NULL,
  `Year` varchar(4) COLLATE utf16_bin NOT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `year`
--

INSERT INTO `year` (`Id`, `Year`, `Created`) VALUES
(1, '2018', '2018-01-06 05:07:58'),
(2, '2019', '2018-01-06 05:08:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `carinformation`
--
ALTER TABLE `carinformation`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `cost`
--
ALTER TABLE `cost`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `model`
--
ALTER TABLE `model`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `repairinformation`
--
ALTER TABLE `repairinformation`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `year`
--
ALTER TABLE `year`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `carinformation`
--
ALTER TABLE `carinformation`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cost`
--
ALTER TABLE `cost`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `model`
--
ALTER TABLE `model`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `repairinformation`
--
ALTER TABLE `repairinformation`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `year`
--
ALTER TABLE `year`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
